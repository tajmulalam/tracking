package com.xoranex.tracking;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TAG";
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onSharedIntent();
    }

    private void onSharedIntent() {
        Intent receiverdIntent = getIntent();
        String receivedAction = receiverdIntent.getAction();
        String receivedType = receiverdIntent.getType();

        if (receivedAction.equals(Intent.ACTION_SEND)) {

            // check mime type
            if (receivedType.startsWith("text/")) {

                String receivedText = receiverdIntent
                        .getStringExtra(Intent.EXTRA_TEXT);
                if (receivedText != null) {
                    //do your stuff             https://xorao.biz/ctt/#OC034129312PT
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://xorao.biz/ctt/#" + receivedText));
                    startActivity(browserIntent);
                    finish();
                }
            } else if (receivedType.startsWith("image/")) {

                Uri receiveUri = (Uri) receiverdIntent
                        .getParcelableExtra(Intent.EXTRA_STREAM);

                if (receiveUri != null) {
                    //do your stuff
                    fileUri = receiveUri;// save to your own Uri object

                    Log.e(TAG, receiveUri.toString());
                }
            }

        } else if (receivedAction.equals(Intent.ACTION_MAIN)) {

            Log.e(TAG, "onSharedIntent: nothing shared");
            Toast.makeText(this, "Nothing shared", Toast.LENGTH_SHORT).show();
        }
    }
}
